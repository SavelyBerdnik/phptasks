<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <header class="page-header">
        <img class="page-header__polytech-logo" src="logo.jpg" alt="Логотип МосПолитеха">
        <h1 class="page-header__task-name">"Hello, World!"</h1>
    </header>
    <main class="page-main">
        <?php 
            echo "Hello, World!";
        ?>
    </main>
    <footer class="page-footer">
        <p class="page-footer__task-description">
            Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг. 
        </p>
    </footer>
</body>
</html>
